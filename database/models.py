from sqlalchemy import Column, Integer, String

from database.session import Base

schema = {'schema': 'tugas2'}


class Mahasiswa(Base):
    __tablename__ = 'mahasiswa'
    __table_args__ = schema

    nama = Column(String, primary_key=True)
    npm = Column(String)
    alamat = Column(String)
