from typing import Optional

from sqlalchemy.orm import Session

from database import models
from lab4.schema import Mahasiswa, UpdateMahasiswaRequest


def get_mahasiswa_by_npm(session: Session, npm: str) -> Optional[Mahasiswa]:
    mahasiswa = session.query(models.Mahasiswa).filter(models.Mahasiswa.npm == npm).first()
    if mahasiswa:
        return Mahasiswa(npm=mahasiswa.npm, nama=mahasiswa.nama, alamat=mahasiswa.alamat)


def post_mahasiswa(session: Session, mahasiswa: Mahasiswa) -> Mahasiswa:
    mahasiswa = models.Mahasiswa(**mahasiswa.dict())
    session.add(mahasiswa)
    session.commit()
    session.refresh(mahasiswa)
    return Mahasiswa(npm=mahasiswa.npm, nama=mahasiswa.nama, alamat=mahasiswa.alamat)


def update_mahasiswa(session: Session, npm: str, update_data: UpdateMahasiswaRequest) -> Mahasiswa:
    mahasiswa = session.query(models.Mahasiswa).filter(models.Mahasiswa.npm == npm).first()
    mahasiswa.nama = update_data.nama if update_data.nama else mahasiswa.nama
    mahasiswa.alamat = update_data.alamat if update_data.alamat else update_data.alamat
    session.commit()
    return Mahasiswa(npm=mahasiswa.npm, nama=mahasiswa.nama, alamat=mahasiswa.alamat)


def delete_mahasiswa(session: Session, npm: str) -> None:
    session.query(models.Mahasiswa).filter(models.Mahasiswa.npm == npm).delete()
    session.commit()
