-- liquibase formatted sql

--changeset ryo.axtonlie:create-table-mahasiswa
CREATE TABLE tugas2.mahasiswa (
    npm varchar NOT NULL PRIMARY KEY,
    nama varchar NOT NULL,
    alamat varchar NOT NULL
);
