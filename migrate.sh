sudo docker run --net=host --rm -v "$(pwd)/migrations/changelog:/liquibase/changelog" liquibase/liquibase \
    --url="jdbc:postgresql://localhost:5432/tugas2?currentSchema=tugas2" \
    --logLevel=debug \
    --changeLogFile=changelog/migration.sql \
    --username=postgres \
    --password=password update
