from typing import Optional, Dict, Set

import aiofiles as aiofiles
from fastapi import FastAPI, File, UploadFile, Depends
from pydantic import BaseModel
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from database import models
from database.database_accessor import get_mahasiswa_by_npm, post_mahasiswa, update_mahasiswa, delete_mahasiswa
from database.session import engine, get_session
from lab4.schema import Mahasiswa, UpdateMahasiswaRequest

models.Base.metadata.create_all(bind=engine)
app = FastAPI()

image_id_to_path_dict: Dict[int, str] = {}


@app.get("/mahasiswa/{npm}", response_model=Mahasiswa)
def get_mahasiswa_by_npm_request(npm: str, session: Session = Depends(get_session)):
    mahasiswa = get_mahasiswa_by_npm(session=session, npm=npm)
    if not mahasiswa:
        return JSONResponse(status_code=404, content={'error_message': f'Mahasiswa dengan NPM: {npm} tidak ada!'})
    return mahasiswa


@app.post("/mahasiswa", response_model=Mahasiswa)
def post_mahasiswa_request(mahasiswa_data: Mahasiswa, session: Session = Depends(get_session)):
    mahasiswa = get_mahasiswa_by_npm(session=session, npm=mahasiswa_data.npm)
    if mahasiswa:
        return JSONResponse(status_code=422, content={'error_message': f'Mahasiwa dengan NPM: {mahasiswa_data.npm} sudah ada!'})
    new_mahasiswa = Mahasiswa(nama=mahasiswa_data.nama, alamat=mahasiswa_data.alamat, npm=mahasiswa_data.npm)
    inserted_mahasiswa = post_mahasiswa(session=session, mahasiswa=new_mahasiswa)
    return inserted_mahasiswa



@app.put("/mahasiswa/{npm}", response_model=Mahasiswa)
def update_mahasiswa_request(npm: str, update_mahasiswa_data: UpdateMahasiswaRequest, session: Session = Depends(get_session)):
    mahasiswa = get_mahasiswa_by_npm(session=session, npm=npm)
    if not mahasiswa:
        return JSONResponse(status_code=404, content={'error_message': f'Mahasiswa dengan NPM: {npm} tidak ada!'})
    updated_mahasiswa = update_mahasiswa(session=session, npm=npm, update_data=update_mahasiswa_data)
    return updated_mahasiswa


@app.delete("/mahasiswa/{npm}")
def delete_mahasiswa_request(npm: str, session: Session = Depends(get_session)):
    mahasiswa = get_mahasiswa_by_npm(session=session, npm=npm)
    if not mahasiswa:
        return JSONResponse(status_code=404, content={'error_message': f'Mahasiswa dengan NPM: {npm} tidak ada!'})
    delete_mahasiswa(session=session, npm=npm)
    return JSONResponse(status_code=200, content={'message': f'Delete Mahasiswa dengan NPM: {npm} berhasil'})


@app.post("/images")
async def upload_images(image: UploadFile = File(...)):
    if image.content_type != 'image/jpeg':
        return JSONResponse(status_code=403, content={
            'error_message': 'File yang harus di upload harus berupa Image dengan format jpg!'})
    image_id = len(image_id_to_path_dict) + 1
    async with aiofiles.open(f"./images/{image_id}.jpg", "wb") as file:
        image_content = await image.read()
        await file.write(image_content)
    return JSONResponse(status_code=200, content={'message': 'success'})
