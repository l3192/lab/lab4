from typing import Optional

from pydantic import BaseModel


class Mahasiswa(BaseModel):
    nama: str
    npm: str
    alamat: str


class UpdateMahasiswaRequest(BaseModel):
    nama: Optional[str] = None
    alamat: Optional[str] = None
